import { combineReducers } from 'redux';
import { combineForms, modelReducer, formReducer } from 'react-redux-form';

const initialItemListState = require('../sample-data');

/**
 * Example reducer function
 * @param {Object} state  - Previous state
 * @param {Object} action - Action to perform.
 * @returns {Object}      - Next state.
 */
function reducer(state = {}, action = {}) {
  switch (action.type) {
    case 'ACTION':
      return {};
    default:
      return state;
  }
}


const deneme = modelReducer('itemList', initialItemListState.itemList);

const emptyLine = { id: '', code: '', name: '', description: '', unitCost: 0, unit: '', quantity: 0, discountPercent: 0, taxPercent: 0 };

function itemListReducer(state = {}, action = {}) {

  const newState = deneme(state, action);
  let items = [];


  switch (action.type) {
    case 'REMOVE_LINE':
      items = [...newState.items];
      items.splice(action.payload.index, 1);
      return {
        ...newState,
        items,
      };
    case 'ADD_LINE':
      items = [...newState.items, emptyLine];
      return {
        ...newState,
        items,
      };
    default:
      return newState;
  }
}



const rootReducer = combineReducers({
  itemList: itemListReducer,
  //itemList: modelReducer('itemList', initialItemListState.itemList),
  itemListForm: formReducer('itemList', initialItemListState.itemList),
}, );

export default rootReducer;
