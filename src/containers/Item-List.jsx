import React from 'react';
import { connect } from 'react-redux';

import ItemList from '../components/item-list';

import { removeLine, addLine } from '../actions';


const mapStateToProps = (state) => {
  return {
    items: state.itemList.items,
    modelBase: 'itemList',
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onRemoveLine: (index) => dispatch(removeLine(index)),
    onAddLine: () => dispatch(addLine()),
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(ItemList);
