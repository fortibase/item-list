import React from 'react';

export default class Dummy extends React.PureComponent {
  render() {
    return (
      <div>
        DUMMY!
      </div>
    );
  }
}
