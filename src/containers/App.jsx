// TODO: Replace `extends React.Component` with `extends React.PureComponent` for pure components. PureComponents are not supported with flow-bin, eslint-plugin-react and WebStorm libraries.

import React from 'react';
import { connect } from 'react-redux';
import ItemList from '../containers/Item-List';

const initalData = require('../sample-data');

const App = ({state}) =>
  <div>
    <ItemList modelBase="itemList" items={initalData.itemList.items} />
    <div>State3: <pre>{state}</pre></div>
  </div>
;

function mapState(state) {
  return {
  //   state: JSON.stringify(state, null, 4),
  };
}

export default connect(mapState)(App);
