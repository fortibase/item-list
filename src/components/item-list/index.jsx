import React from 'react';
import { Form, Field } from 'react-redux-form';
import Lines from './Lines';


function calculateLineTotal(item) {
  const { quantity = 0, unitCost = 0, discountPercent = 0, taxPercent = 0 } = item;
  const totalBeforeTax = quantity * unitCost * (1 - (discountPercent / 100));
  const tax = totalBeforeTax * (taxPercent / 100);
  const totalAfterTax = totalBeforeTax + tax;
  return { totalBeforeTax, totalAfterTax, tax };
}

function calculateGrandTotal(items) {
  let grandTotalBeforeTax = 0;
  let grandTotalAfterTax = 0;

  for (let item of items) {
    const {totalBeforeTax, totalAfterTax} = calculateLineTotal(item);
    grandTotalBeforeTax += totalBeforeTax;
    grandTotalAfterTax += totalAfterTax;
  }

  const totalTax = grandTotalAfterTax - grandTotalBeforeTax;

  return { grandTotalBeforeTax, grandTotalAfterTax, totalTax };
}

const ItemList = ({ modelBase = 'itemList', items, precision = 2, onRemoveLine, onAddLine }) => (
  <div>
    <Form model={modelBase}>
      <Field model={`${modelBase}.name`}>
        <label>Name</label>
        <input type="text" />
      </Field>
      <Field model={`${modelBase}.address`}>
        <label>Address</label>
        <input type="text" />
      </Field>

      <Lines
        onRemoveLine={onRemoveLine}
        onAddLine={onAddLine}
        modelBase={modelBase}
        items={items}
        calculateLineTotal={calculateLineTotal}
        precision={precision}
        {...calculateGrandTotal(items)}
      />

    </Form>
  </div>
);

export default ItemList;
