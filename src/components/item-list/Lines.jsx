import React from 'react';
import { Field, track } from 'react-redux-form';

const columnNumber = 8;


class Line extends React.Component {
  removeLine = () => {
    return this.props.onRemoveLine(this.props.index);
  };


  // TODO: Sayı eklediğinde toplam hanesi genişleyince render yavaşlıyor çünkü tüm tablo yeniden çiziliyor. Orayı fixleyince update="onBlur" kaldırılabilir.
  render() {
    let { modelBase, item, index, precision, calculateLineTotal, onRemoveLine} = this.props;
    return (
      <tr>
        <td><Field updateOn="blur" model={track(`${modelBase}.items[].name`, { id: item.id })}> <input type="text" /></Field></td>
        <td><Field model={track(`${modelBase}.items[].quantity`, { id: item.id })}><input type="text" /></Field></td>
        <td><Field updateOn="blur" model={track(`${modelBase}.items[].unitCost`, { id: item.id })}><input type="text" /></Field></td>
        <td><Field updateOn="blur" model={track(`${modelBase}.items[].discountPercent`, { id: item.id })}><input type="text" /></Field></td>
        <td><Field updateOn="blur" model={track(`${modelBase}.items[].taxPercent`, { id: item.id })}><input type="text" /></Field></td>
        <td style={{ textAlign: 'right', width: '100px' }}> {calculateLineTotal(item).tax.toFixed(precision)} </td>
        <td style={{ textAlign: 'right', paddingLeft: '20px', width: '100px' }}> {calculateLineTotal(item).totalAfterTax.toFixed(precision)} </td>
        <td><button type="button" onClick={this.removeLine}>Remove</button></td>
      </tr>
    );
  }
}


const Lines = (props) => {
  let {modelBase, items, calculateLineTotal, grandTotalBeforeTax, grandTotalAfterTax, totalTax, precision, onRemoveLine, onAddLine} = props;
  return (
    <div>
      <table>
        <thead>
        <tr>
          <th>Product</th>
          <th>Quantity</th>
          <th>Price</th>
          <th>Discount</th>
          <th>Tax</th>
          <th>Tax</th>
          <th>Total</th>
          <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        {items.map((item, index) =>
          <Line {...props} key={index} item={item} index={index}/>
        )}

        <tr>
          <td colSpan={columnNumber - 2} style={{textAlign: 'right'}}>Sub Total</td>
          <td style={{ textAlign: 'right' }}>{ grandTotalBeforeTax.toFixed(precision) }</td>
          <td> &nbsp; </td>
        </tr>

        <tr>
          <td colSpan={columnNumber - 2} style={{textAlign: 'right'}}>Tax</td>
          <td style={{ textAlign: 'right' }}>{ totalTax.toFixed(precision) }</td>
          <td> &nbsp; </td>
        </tr>

        <tr>
          <td colSpan={columnNumber - 2} style={{textAlign: 'right'}}>Total</td>
          <td style={{ textAlign: 'right' }}>{ grandTotalAfterTax.toFixed(precision) }</td>
          <td> &nbsp; </td>
        </tr>
        <tr>
          <td colSpan={ columnNumber }>
            <button type="button" onClick={() => onAddLine()}>Add Line</button>
          </td>
        </tr>
        </tbody>
      </table>
    </div>
  );
};

export default Lines;

{/*<tr key={index}>*/}
{/*<td><Deneme a={index} b={0} /><Field updateOn="blur" model={track(`${modelBase}.items[].name`, { id: items[index].id })}> <input type="text" /></Field></td>*/}
{/*<td><Field updateOn="blur" model={track(`${modelBase}.items[].quantity`, { id: items[index].id })}><input type="text" /></Field></td>*/}
{/*<td><Field updateOn="blur" model={track(`${modelBase}.items[].unitCost`, { id: items[index].id })}><input type="text" /></Field></td>*/}
{/*<td><Field updateOn="blur" model={track(`${modelBase}.items[].discountPercent`, { id: items[index].id })}><input type="text" /></Field></td>*/}
{/*<td><Field updateOn="blur" model={track(`${modelBase}.items[].taxPercent`, { id: items[index].id })}><input type="text" /></Field></td>*/}
{/*<td style={{ textAlign: 'right' }}> {calculateLineTotal(item).tax.toFixed(precision)} </td>*/}
{/*<td style={{ textAlign: 'right', paddingLeft: '20px' }}> {calculateLineTotal(item).totalAfterTax.toFixed(precision)} </td>*/}
{/*<td><button type="button" onClick={() => onRemoveLine(index)}>Sil</button></td>*/}
{/*</tr>*/}
