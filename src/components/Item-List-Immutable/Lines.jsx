import React from 'react';
import { Field, track } from 'react-redux-form';

const columnNumber = 8;


class Line extends React.PureComponent {
  removeLine = () => {
    return this.props.onRemoveLine(this.props.index);
  };

  render() {
    let { modelBase, item, index, precision, calculateLineTotal, onRemoveLine} = this.props;
    return (
      <tr>
        <td><Field updateOn="blur" model={track(`${modelBase}.items[].name`, { id: item.get('id') })}> <input type="text" /></Field></td>
        <td><Field updateOn="blur" model={track(`${modelBase}.items[].quantity`, { id: item.get('id') })}><input type="text" /></Field></td>
        <td><Field updateOn="blur" model={track(`${modelBase}.items[].unitCost`, { id: item.get('id') })}><input type="text" /></Field></td>
        <td><Field updateOn="blur" model={track(`${modelBase}.items[].discountPercent`, { id: item.get('id') })}><input type="text" /></Field></td>
        <td><Field updateOn="blur" model={track(`${modelBase}.items[].taxPercent`, { id: item.get('id') })}><input type="text" /></Field></td>
        <td style={{ textAlign: 'right' }}> {calculateLineTotal(item).tax.toFixed(precision)} </td>
        <td style={{ textAlign: 'right', paddingLeft: '20px' }}> {calculateLineTotal(item).totalAfterTax.toFixed(precision)} </td>
        <td><button type="button" onClick={this.removeLine}>Sil</button></td>
      </tr>
    );
  }
}

const Lines = (props) => {
  let {modelBase, items, calculateLineTotal, grandTotalBeforeTax, grandTotalAfterTax, totalTax, precision, onRemoveLine, onAddLine} = props;
  return (
    <div>
      <table>
        <thead>
        <tr>
          <th>Ürün</th>
          <th>Miktar</th>
          <th>Fiyat</th>
          <th>İndirim</th>
          <th>Vergi</th>
          <th>Vergi</th>
          <th>Toplam</th>
          <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        {items.map((item, index) =>
          <Line {...props} key={index} item={item} index={index}/>
        )}

        <tr>
          <td colSpan={columnNumber - 2} style={{textAlign: 'right'}}>Ara Toplam</td>
          <td style={{textAlign: 'right'}}>{ grandTotalBeforeTax.toFixed(precision) }</td>
          <td> &nbsp; </td>
        </tr>

        <tr>
          <td colSpan={columnNumber - 2} style={{textAlign: 'right'}}>Vergi</td>
          <td style={{textAlign: 'right'}}>{ totalTax.toFixed(precision) }</td>
          <td> &nbsp; </td>
        </tr>

        <tr>
          <td colSpan={columnNumber - 2} style={{textAlign: 'right'}}>Toplam</td>
          <td style={{textAlign: 'right'}}>{ grandTotalAfterTax.toFixed(precision) }</td>
          <td> &nbsp; </td>
        </tr>
        <tr>
          <td colSpan={ columnNumber }>
            <button type="button" onClick={() => onAddLine()}>Satır Ekle</button>
          </td>
        </tr>
        </tbody>
      </table>
    </div>
  );
};

export default Lines;
