import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import App from './containers/App';

const initialState = require('./sample-data');
const store = configureStore(initialState);

render(
  <AppContainer>
    <Provider store={store}>
      <App />
    </Provider>
  </AppContainer>,
  document.getElementById('app')
);

if (module.hot) {
  module.hot.accept('./containers/App', () => {
    const HotApp = require('./containers/App').default; // eslint-disable-line global-require

    render(
      <AppContainer>
        <Provider store={store}>
          <HotApp />
        </Provider>
      </AppContainer>,
      document.getElementById('app')
    );
  });
}
