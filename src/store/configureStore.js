import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';

/**
 * Creates and returns redux store with initial values.
 * @param   {any} [initialState]  - Initial state for redux store.
 * @returns {Object}              - Store.
 */
export default function configureStore(initialState) {
  const store = applyMiddleware(thunk)(createStore)(rootReducer, initialState, window.devToolsExtension ? window.devToolsExtension() : f => f);

  if (process.env.NODE_ENV === 'development' && module.hot) {
    module.hot.accept('../reducers', () =>
      store.replaceReducer(require('../reducers').default) // eslint-disable-line global-require
    );
  }

  return store;
}
