export const removeLine = (index) => ({
  type: 'REMOVE_LINE',
  payload: {
    index,
  },
});

export const addLine = () => ({
  type: 'ADD_LINE',
});

