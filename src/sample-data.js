module.exports = {
  itemList: {
    name: 'Özüm Eldoğan',
    address: 'İstanbul',
    items: [
      { id: 12, code: 'KA-12', name: 'Kalem', description: 'Yazı', unitCost: 12.95, unit: 'Adet', quantity: 2, discountPercent: 0, taxPercent: 18 },
      { id: 13, code: 'SI-02', name: 'Silgi', description: 'Silmek', unitCost: 2.95, unit: 'Adet', quantity: 1, discountPercent: 0, taxPercent: 18 },
      { id: 14, code: 'KA-13', name: 'Pergel', description: 'Açı Ölçmek', unitCost: 12.95, unit: 'Adet', quantity: 2, discountPercent: 0, taxPercent: 18 },
      { id: 15, code: 'SI-03', name: 'Cetvel', description: 'Çizgi', unitCost: 2.95, unit: 'Adet', quantity: 1, discountPercent: 0, taxPercent: 18 },

      { id: 16, code: 'KA-14', name: 'Tebeşir', description: 'Tahta', unitCost: 12.95, unit: 'Adet', quantity: 2, discountPercent: 0, taxPercent: 18 },
      { id: 17, code: 'SI-04', name: 'Kağıt', description: 'Üstüne yaz', unitCost: 2.95, unit: 'Adet', quantity: 1, discountPercent: 0, taxPercent: 18 },

      { id: 18, code: 'KA-15', name: 'Defter', description: 'Matematik', unitCost: 12.95, unit: 'Adet', quantity: 2, discountPercent: 0, taxPercent: 18 },
      { id: 19, code: 'SI-05', name: 'Abaküs', description: 'Sayaç', unitCost: 2.95, unit: 'Adet', quantity: 1, discountPercent: 0, taxPercent: 18 },

      { id: 20, code: 'KA-16', name: 'Divit', description: 'Güzel Yazı', unitCost: 12.95, unit: 'Adet', quantity: 2, discountPercent: 0, taxPercent: 18 },
      { id: 21, code: 'SI-06', name: 'Çanta', description: 'Okul için', unitCost: 2.95, unit: 'Adet', quantity: 1, discountPercent: 0, taxPercent: 18 },


    ],
  },
};
