// Helper file for chai tests, which should be executed before every test.

import jsdom from 'jsdom';
import chai from 'chai';
import chaiImmutable from 'chai-immutable';
import dirtyChai from 'dirty-chai';

// Create jsdom versions of the document and window objects that would normally be provided by the web browser.
const doc = jsdom.jsdom('<!doctype html><html><body></body></html>');
const win = doc.defaultView;

// Put them on the global object, so that they can be accessed by React
global.document = doc;
global.window = win;

// Take all the properties that the jsdom window object contains, such as navigator, and hoist them on to the Node.js global object.
// This is done so that properties provided by window can be used without the 'window' prefix, which is what would happen in a browser environment.
Object.keys(window).forEach((key) => {
  if (!(key in global)) {
    global[key] = window[key];
  }
});

chai.use(chaiImmutable);
chai.use(dirtyChai); // Adds function form for terminating assertion properties. i.e. expect().to.be.ok -> expect().to.be.ok. For Eslint 'no-unused-expressions' rule.
