const webpack = require('webpack');                     // eslint-disable-line import/no-extraneous-dependencies
const WebpackDevServer = require('webpack-dev-server'); // eslint-disable-line import/no-extraneous-dependencies
const config = require('./webpack.config');

new WebpackDevServer(webpack(config), {
  publicPath: config.output.publicPath,
  hot: true,
  historyApiFallback: true,
  stats: {
    colors: true,
  },
}).listen(8080, 'localhost', (err) => {
  if (err) {
    console.log(err);                                   // eslint-disable-line no-console
  }

  console.log('Listening at localhost:8080');           // eslint-disable-line no-console
});
